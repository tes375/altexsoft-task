import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { checkFormDirty } from './FormLib.helpers';
import { useFormLibContext } from './FormLib.context';
import { formReducer, initFormReducer } from './FormLib.reducer';
import {
  ACTION_RESET_FORM,
  ACTION_SUBMIT_FORM,
  ACTION_SET_FORM_DIRTY,
  ACTION_SET_FIELD_VALUE,
  ACTION_SET_FIELD_TOUCHED,
  ACTION_SET_SUBMITTING_PROCESS,
} from './FormLib.actions';

export const useFormLib = ({
  onReset,
  onSubmit,
  apiErrors,
  initialErrors,
  initialTouched,
  isValidInitial,
  hasResetOnSubmit,
  // rules = [],
  initialValues = {},
  isLoading = false,
}) => {
  const initialReducerProps = useMemo(
    () => ({ initialValues, initialErrors, initialTouched, isValidInitial }),
    [initialErrors, initialTouched, initialValues, isValidInitial]
  );

  const [formState, dispatchForm] = useReducer(formReducer, initialReducerProps, initFormReducer);

  /**
   * Setters
   */
  const setFieldValue = (fieldName, fieldValue) =>
    dispatchForm({ type: ACTION_SET_FIELD_VALUE, payload: { fieldName, fieldValue } });

  const setFieldTouched = (fieldName, fieldTouched) => {
    dispatchForm({ type: ACTION_SET_FIELD_TOUCHED, payload: { fieldName, fieldTouched } });
  };

  const setSubmittingInProcess = isSubmittingInProcess => {
    dispatchForm({ type: ACTION_SET_SUBMITTING_PROCESS, payload: { isSubmittingInProcess } });
  };

  const setFormDirty = isDirty =>
    dispatchForm({ type: ACTION_SET_FORM_DIRTY, payload: { isDirty } });

  const setFormSubmission = () => dispatchForm({ type: ACTION_SUBMIT_FORM });

  const setFormInitial = useCallback(
    (nextState = initialValues) =>
      dispatchForm({
        type: ACTION_RESET_FORM,
        payload: { ...initialReducerProps, initialValues: nextState },
      }),
    [initialReducerProps, initialValues]
  );

  /**
   * Handlers
   */
  const submitForm = () => {
    setFormSubmission();

    setSubmittingInProcess(true);

    onSubmit(formState.values);
  };

  const handleReset = event => {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    if (event && event.stopPropagation) {
      event.stopPropagation();
    }

    onReset?.();

    return setFormInitial();
  };

  const handleSubmit = event => {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    if (event && event.stopPropagation) {
      event.stopPropagation();
    }

    return submitForm();
  };

  const handleBlur = fieldName => () => {
    setFieldTouched(fieldName, true);
  };

  const handleChange = fieldName => fieldValue => {
    setFieldValue(fieldName, fieldValue);
    setFormDirty(checkFormDirty(formState.values, initialValues));
  };

  /**
   * Getters
   */
  const getFieldProps = fieldName => ({
    name: fieldName,
    value: formState.values[fieldName],
    onBlur: handleBlur(fieldName),
    onChange: handleChange(fieldName),
  });

  const getFieldMeta = fieldName => ({
    value: formState.values[fieldName],
    errors: formState.errors[fieldName],
    touched: formState.touched[fieldName],
  });

  const getFieldHelpers = fieldName => ({
    setValue: fieldValue => setFieldValue(fieldName, fieldValue),
    setTouched: fieldTouched => setFieldTouched(fieldName, fieldTouched),
  });

  useEffect(() => {
    if (hasResetOnSubmit && !isLoading && formState.isSubmittingInProcess) {
      setFormInitial();
    }

    if (!isLoading && formState.isSubmittingInProcess) {
      setSubmittingInProcess(false);
    }
  }, [isLoading, setFormInitial, hasResetOnSubmit, formState.isSubmittingInProcess]);

  return {
    ...formState,
    apiErrors,
    initialValues,
    initialTouched,
    getFieldProps,
    getFieldMeta,
    getFieldHelpers,
    setFormDirty,
    setFieldValue,
    setFieldTouched,
    setFormSubmission,
    submitForm,
    handleReset,
    handleSubmit,
    resetForm: setFormInitial,
  };
};

export const useField = fieldName => {
  const { getFieldMeta, getFieldProps, getFieldHelpers } = useFormLibContext();

  return [getFieldProps(fieldName), getFieldMeta(fieldName), getFieldHelpers(fieldName)];
};
